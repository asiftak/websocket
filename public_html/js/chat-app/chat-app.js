// Change localhost to the name or ip address of the host running the chat server
var conn = conn;

function displayChatMessage(from, message) {
    var node = document.createElement("LI");

    if (from) {
        var nameNode = document.createElement("STRONG");
        var nameTextNode = document.createTextNode(from);
        nameNode.appendChild(nameTextNode);
        node.appendChild(nameNode);
    }

    var messageTextNode = document.createTextNode(message);
    node.appendChild(messageTextNode);

    document.getElementById("messageList").appendChild(node);
}

function displayUserTypingMessage(from) {
    var nodeId = 'userTyping'+from.name.replace(' ','');
    var node = document.getElementById(nodeId);
    if (!node) {
        node = document.createElement("LI");
        node.id = nodeId;

        var messageTextNode = document.createTextNode(from.name + ' is typing...');
        node.appendChild(messageTextNode);

        document.getElementById("messageList").appendChild(node);
    }
}

function removeUserTypingMessage(from) {
    var nodeId = 'userTyping' + from.name.replace(' ', '');
    var node = document.getElementById(nodeId);
    if (node) {
        node.parentNode.removeChild(node);
    }
}

var conn;

function connectToChat(to_id, name, from_id) {
    conn = new WebSocket(chatUrl);

    conn.onopen = function() {
        var params = {
            'roomId': to_id+'_'+from_id,
            'userName': name,
            'action': 'connect'
        };
        console.log(params);
        conn.send(JSON.stringify(params));
    };

    conn.onmessage = function(e) {
        console.log(e);
        var data = JSON.parse(e.data);

        if (data.hasOwnProperty('message') && data.hasOwnProperty('from')) {
            displayChatMessage(data.from.name, data.message);
        }
        else if (data.hasOwnProperty('message')) {
            displayChatMessage(null, data.message);
        }
        else if (data.hasOwnProperty('type')) {
            if (data.type == 'list-users' && data.hasOwnProperty('clients')) {
                displayChatMessage(null, 'There are ' + data.clients.length + ' users connected');
            }
            else if (data.type == 'user-started-typing') {
                displayUserTypingMessage(data.from)
            }
            else if (data.type == 'user-stopped-typing') {
                removeUserTypingMessage(data.from);
            }
        }
    };

    conn.onerror = function(e) {
        console.log(e);
    };

    return false;
}
    

$.urlParam = function(name,url){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(url);
	return results[1] || 0;
}

function GetURLParameter(sParam, url)
{
    var sPageURL = url;
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}

function sendChatMessage(message) {
    
    dataObj = {};
    console.log(message);
    var params = {
        'message': message,
    };
    
    
    conn.call('chat_message',JSON.stringify(params)).then(
        function (res) {
            console.log("Result:", res);
        }
    );
    
    return dataObj['message'];
}

function updateChatTyping() {
    var params = {};

    if ($("#input-message").val().length > 0) {
        params = {
            'action': 'Typing...',
        };
        
        conn.call('chat',JSON.stringify(params)).then(
            function (res) {
                console.log("Result:", res);
            }
        );
    }

}

function activeUserListing(saas_customer) {
   var params = {};
   
        params = {
            'action': 'user_online_ofline_status',
            'saas_customer':saas_customer,
        };
        conn.call('user_online_ofline_status',JSON.stringify(params)).then(
            function (res) {
                //console.log("Result:", res);
            }
        );
    
}
