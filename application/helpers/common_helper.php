<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



function ratchet_server()
{
    
    $CI = & get_instance();
    try
    {
        require_once (APPPATH . 'libraries/Ratchet/vendor/autoload.php');
        require_once (APPPATH . 'libraries/Ratchet/src/ChatApp/Pusher.php');



        $loop = React\EventLoop\Factory::create();
        $pusher = new ChatApp\Pusher($CI);

        // Listen for the web server to make a ZeroMQ push after an ajax request
        $context = new React\ZMQ\Context($loop);
        $pull = $context->getSocket(ZMQ::SOCKET_PULL);

        $pull->bind('tcp://127.0.0.1:5555'); // Binding to 127.0.0.1 means the only client that can connect is itself
        
        $pull->on('message', array($pusher, 'onsendResult'));

        // Set up our WebSocket server for clients wanting real-time updates
        $webSock = new React\Socket\Server("0.0.0.0:8888", $loop); // Binding to 0.0.0.0 means remotes can connect
        $webServer = new Ratchet\Server\IoServer(
                new Ratchet\Http\HttpServer(
                new Ratchet\WebSocket\WsServer(
                new Ratchet\Wamp\WampServer(
                $pusher
                )
                )
                ), $webSock
        );

        $loop->run();
    }
    catch (Exception $e)
    {
        // write to system log
        $CI->log_model->add('error', $e->getMessage(),false,$config[ENVIRONMENT]['database_details']);

        $result = array('status' => false, 'message' => lang('msg_something_went_wrong'));
        $CI->response($result, 500);
    }
}