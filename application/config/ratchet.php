<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$config['ratchet_config']['local'] = array( 
    "database_details" => array(
        "dsn"=> "",
        "hostname"=> "localhost",
        "username"=> "root",
        "password"=> "123456",
        "database"=> "ratchet_db",
        "dbdriver"=> "mysqli",
        "dbprefix"=> "",
        "pconnect"=> false,
        "db_debug"=> true,
        "cache_on"=> false,
        "cachedir"=> "",
        "char_set"=> "utf8",
        "dbcollat"=> "utf8_general_ci",
        "swap_pre"=> "",
        "encrypt"=> false,
        "compress"=> false,
        "stricton"=> false,
        "failover"=> "",
        "save_queries"=> true
    ),
    "zmq_port" => "5555",
    "zmq_url" => "127.0.0.1",
    "wss_port" => "8888",
    );