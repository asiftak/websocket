<!DOCTYPE HTML>
<html>
    <head>
        <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?= base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script src="<?= base_url()?>public_html/js/jquery.min.js"></script>
        <script src="<?= base_url()?>public_html/js/when.js"></script>
        <title>Web chat - Websockets</title>
    </head>
    <body>
        <header style="background-color: #333333;padding: 15px;">
            <h3 class="text-center headertext" style="margin: 0px;">Chat App</h3>
        </header>
        <content>
            <div class="col-sm-4"></div>
            <div class="col-sm-4 col-xs-12 mainbox" style="padding:0px;">
                <div class="chathead">
                    <a href="<?= base_url(); ?>welcome" style="color: black;">Some Name</a>
                    <div class="pull-right"><img src="<?= base_url(); ?>assets/images/bhaveshfwl.jpg" class="dp"></div>
                    <div class="clearfix"></div>
                </div>
                <div class="chatbox">
                    <div class="message"><font>Hello</font></div><div class="clearfix"></div>
                    <div class="message-sent"><font>Hello</font></div><div class="clearfix"></div>
                    <div class="message"><font>Some message</font></div><div class="clearfix"></div>
                </div>
                <div class="message-input col-sm-4 col-xs-12">
                    <input type="text" placeholder="Write a message..." id="input-message">
                    <button class="send-button"><i class='fa fa-heart'></i></button>
                </div>
            </div>
            <div class="col-sm-4"></div>
        </content>
        <div class="clearfix"></div>
        <footer class="footer">Copyright &copy; wchat.com</footer>
        <script type="text/javascript">
            window.onresize = chatScrollHeight;
            chatScrollHeight();
            function chatScrollHeight(){
                height = window.innerHeight - 53 - 55 - 30 - 41 + "px";
                $(".chatbox").css("height", height)
            }
        </script>
        <script src="<?= base_url()?>public_html/autobahn.js"></script>
        <script src="<?= base_url()?>public_html/js/chat-app/chat-app.js"></script>
<script>
    var conn = new ab.Session('ws://localhost:8585',
        function() {
            conn.subscribe('chat', function(topic, data) {
                // This is where you would add the new article to the DOM (beyond the scope of this tutorial)
                console.log(data);
            });
            conn.subscribe('chat_message', function(topic, data) {
                // This is where you would add the new article to the DOM (beyond the scope of this tutorial)
                data = JSON.parse(data);
                console.log(data);
                $(".chatbox").append("<div class='message'><font>"+data.message+"</font></div><div class='clearfix'></div>");
            });
        },
        function() {
            console.warn('WebSocket connection closed');
        },
        {'skipSubprotocolCheck': true}
    );
    
    $(document.body).on("keyup", "#input-message", function(e){
        e.preventDefault();
//        var messagelength = $.trim(message).length;
//        if(messagelength > 0)
//        {
//            pushMessage(message);
//        }
        updateChatTyping();
        
      });
</script>
<script>
    $(".send-button").on("click", pushMessage);
    $("#input-message").on("keyup", function(){
        if($("#input-message").val().length > 0){
            $(".send-button").html("Send");
        }
        else{
            $(".send-button").html("<i class='fa fa-heart'></i>");
        }
    });
    
    function pushMessage() {
        if($("#input-message").val().length > 0)
        {
            var sentMessage = $("#input-message").val();
        }
        else
        {
            var sentMessage = "<i class='fa fa-heart'></i>";
        }
        sendChatMessage(sentMessage);
        if(sentMessage.length >= 1){
            $(".chatbox").append("<div class='message-sent'><font>"+sentMessage+"</font></div><div class='clearfix'></div>");
        }
        else{
            $(".chatbox").append("<div class='message-sent'><i class='fa fa-heart'></i></div><div class='clearfix'></div>");
        }
        $("#input-message").val("");
        $(".send-button").html("<i class='fa fa-heart'></i>");
    }
</script>
    </body>
</html>